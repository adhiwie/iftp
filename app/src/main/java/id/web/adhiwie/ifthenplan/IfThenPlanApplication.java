package id.web.adhiwie.ifthenplan;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.estimote.sdk.EstimoteSDK;
import com.google.firebase.database.FirebaseDatabase;

import io.fabric.sdk.android.Fabric;

/**
 * Main {@link Application} object for Demos. It configures EstimoteSDK.
 *
 * @author wiktor@estimote.com (Wiktor Gworek)
 */
public class IfThenPlanApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        FirebaseDatabase.getInstance().setPersistenceEnabled(true);

        Fabric.with(this, new Crashlytics());

        // Initializes Estimote SDK with your App ID and App Token from Estimote Cloud.
        // You can find your App ID and App Token in the
        // Apps section of the Estimote Cloud (http://cloud.estimote.com).
        EstimoteSDK.initialize(this, "ifthenplan", "a96a533f3419100fdda591d0894ad901");

        // Configure verbose debug logging.
        EstimoteSDK.enableDebugLogging(true);
    }

}
