package id.web.adhiwie.ifthenplan.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import id.web.adhiwie.ifthenplan.R;

/**
 * Created by adhiwie on 14/08/15.
 */
public class AddCuesActivity extends BaseActivity {

    private static final String TAG = AddCuesActivity.class.getSimpleName();

    private List<HashMap<String, String>> cuesList;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_add_cues;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                startActivity(new Intent(AddCuesActivity.this, CreateGoalActivity.class));;
            }
        });

        initList();
    }

    private void initList(){
        String[] cuesListItems = getResources().getStringArray(R.array.cues_items);

        int[] cuesImages = new int[]{
                R.drawable.ic_beacon,
                R.drawable.ic_location,
                R.drawable.ic_time
        };

        String[] cuesDesc = new String[]{
                "Beacon provides precise location detection especially inside a building",
                "Location is broader than beacon, it can be anything such as: home, shop, gym, or any specific place",
                "Time acts like an alarm, it will be triggered based on your specified time"
        };

        cuesList = new ArrayList<>();
        for (int i=0; i<cuesListItems.length; i++) {
            HashMap<String, String> cues = new HashMap<>();
            cues.put("cues", cuesListItems[i]);
            cues.put("desc", cuesDesc[i]);
            cues.put("img", Integer.toString(cuesImages[i]));
            cuesList.add(cues);
        }

        String[] from = { "cues","desc","img" };

        // Ids of views in listview_layout
        int[] to = { R.id.cue_name,R.id.cue_desc,R.id.cue_image};

        SimpleAdapter adapter = new SimpleAdapter(this, cuesList,
                R.layout.adapter_add_cues,
                from,to);
        ListView list = (ListView) findViewById(R.id.cues_list);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = null;
                if(position == 0){
                    intent = new Intent(AddCuesActivity.this, ListBeaconsActivity.class);
                    intent.putExtra(ListBeaconsActivity.EXTRAS_TARGET_ACTIVITY, CreateGoalActivity.class.getName());
                    startActivity(intent);
                }else if(position == 1){
                    intent = new Intent(AddCuesActivity.this, AddMapActivity.class);
                    startActivity(intent);
                }else if(position == 2){
                    intent = new Intent(AddCuesActivity.this, AddTimeActivity.class);
                    startActivity(intent);
                }

                startActivity(intent);
            }

        });
    }
}
