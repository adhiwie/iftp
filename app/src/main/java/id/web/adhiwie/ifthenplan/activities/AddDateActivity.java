package id.web.adhiwie.ifthenplan.activities;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;

import java.util.ArrayList;
import java.util.Calendar;

import id.web.adhiwie.ifthenplan.R;

/**
 * Created by adhiwie on 14/08/15.
 */
public class AddDateActivity extends BaseActivity {

    private static final String TAG = AddDateActivity.class.getSimpleName();

    private String date = null;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_add_date;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                startActivity(new Intent(AddDateActivity.this, CreateGoalActivity.class));;
            }
        });

        //confirm button clicked
        View addButton = this.findViewById(R.id.confirm);

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<String> dates = new ArrayList<>();
                dates.add("Date");
                dates.add(date);

                Intent intent = new Intent(AddDateActivity.this, CreateGoalActivity.class);
                intent.putExtra("date", dates);
                startActivity(intent);
            }
        });
    }

    public void setDate(View view) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getFragmentManager(), "datePicker");
    }

    @SuppressLint("ValidFragment")
    public class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            Button dateButton = (Button) getActivity().findViewById(R.id.date);
            date = Integer.toString(day)+"/"+Integer.toString((month+1))+"/"+Integer.toString(year);
            dateButton.setText(date);
        }
    }
}
