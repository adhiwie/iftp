package id.web.adhiwie.ifthenplan.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import id.web.adhiwie.ifthenplan.R;

/**
 * Created by adhiwie on 14/08/15.
 */
public class AddMoveActivity extends BaseActivity {

    private static final String TAG = AddMoveActivity.class.getSimpleName();

    private List<HashMap<String, String>> moveList;
    private ListView list;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_add_cues;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                startActivity(new Intent(AddMoveActivity.this, CreateGoalActivity.class));;
            }
        });

        initList();

        SimpleAdapter adapter = new SimpleAdapter(this, moveList,
                android.R.layout.simple_list_item_1, new String[] {"moves"},
                new int[] {android.R.id.text1});
        list = (ListView) findViewById(R.id.cues_list);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ArrayList<String> movement = new ArrayList<>();
                movement.add("Movement");
                movement.add(list.getItemAtPosition(position).toString());

                Intent intent = new Intent(AddMoveActivity.this, CreateGoalActivity.class);
                intent.putExtra("movement", movement);
                startActivity(intent);
            }

        });
    }

    private void initList(){
        String[] moveListItems = getResources().getStringArray(R.array.move_items);
        moveList = new ArrayList<>();
        for(int i=0; i<moveListItems.length; i++){
            HashMap<String, String> cues = new HashMap<>();
            cues.put("moves", moveListItems[i]);
            moveList.add(cues);
        }

    }
}
