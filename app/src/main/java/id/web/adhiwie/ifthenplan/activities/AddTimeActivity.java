package id.web.adhiwie.ifthenplan.activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;

import id.web.adhiwie.ifthenplan.R;

/**
 * Created by adhiwie on 14/08/15.
 */
public class AddTimeActivity extends BaseActivity {

    private static final String TAG = AddTimeActivity.class.getSimpleName();

    private String time;
    Calendar calendar;
    int day, hour, minute;
    private ArrayList<Integer> selectedDays;
    private StringBuilder repeatStringBuilder;
    private Button timeButton;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_add_time;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                startActivity(new Intent(AddTimeActivity.this, CreateGoalActivity.class));;
            }
        });

        //Initialization
        timeButton = (Button) findViewById(R.id.time);
        calendar = Calendar.getInstance();
        day = calendar.get(Calendar.DAY_OF_WEEK);
        hour = calendar.get(Calendar.HOUR_OF_DAY);
        minute = calendar.get(Calendar.MINUTE);
        repeatStringBuilder = new StringBuilder();
        selectedDays = new ArrayList<>();

        time = String.format("%02d:%02d", hour, minute);
        timeButton.setText(time);

        //confirm button clicked
        View addButton = this.findViewById(R.id.confirm);

        if (addButton != null) {
            addButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(selectedDays.size() < 1){
                        Calendar currentTime = Calendar.getInstance();
                        String[] daysArray = getResources().getStringArray(R.array.days);
                        String days;
                        int currentDay;
                        if(hour < currentTime.get(Calendar.HOUR_OF_DAY)){
                            currentDay = checkDay(currentTime.get(Calendar.DAY_OF_WEEK)) - 1;
                            days = daysArray[currentDay-1];
                        } else if(hour == currentTime.get(Calendar.HOUR_OF_DAY) && minute < currentTime.get(Calendar.MINUTE)) {
                            currentDay = checkDay(currentTime.get(Calendar.DAY_OF_WEEK)) - 1;
                            days = daysArray[currentDay-1];
                        } else {
                            currentDay = currentTime.get(Calendar.DAY_OF_WEEK);
                            days = daysArray[currentDay-1];
                        }
                        repeatStringBuilder = repeatStringBuilder.append(days);
                        selectedDays.add(currentDay-1);
                    }

                    ArrayList<String> times = new ArrayList<>();
                    times.add("Time");
                    times.add(time + " on " + repeatStringBuilder.toString());

                    Intent intent = new Intent(AddTimeActivity.this, CreateGoalActivity.class);
                    intent.putIntegerArrayListExtra("selectedDays", selectedDays);
                    intent.putExtra("time", times);
                    intent.putExtra("hour", hour);
                    intent.putExtra("min", minute);
                    startActivity(intent);
                }
            });
        }

    }

    private int checkDay(int day){
        if(day==1){
            return 8;
        } else {
            return day;
        }
    }

    public void setTime(View v) {
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getFragmentManager(), "timePicker");
    }

    @SuppressLint("ValidFragment")
    public class TimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minuteOfDay) {
            // Do something with the time chosen by the user
            hour = hourOfDay;
            minute = minuteOfDay;

            time = String.format("%02d:%02d", hour, minute);
            timeButton.setText(time);
        }
    }

    public void setSelectedDays(View v){
        DialogFragment repeatDialog = new RepeatDialog();
        repeatDialog.show(getFragmentManager(), "selectedDays");
    }

    @SuppressLint("ValidFragment")
    public class RepeatDialog extends DialogFragment {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final AlertDialog.Builder repeatDialog = new AlertDialog.Builder(getActivity());
            // Set the dialog title
            repeatDialog.setTitle("Select the days")
                    .setMultiChoiceItems(R.array.days, null,
                            new DialogInterface.OnMultiChoiceClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int position,
                                                    boolean isChecked) {
                                    if (isChecked) {
                                        // If the user checked the item, add it to the selected items
                                        selectedDays.add(position);
                                    } else if (selectedDays.contains(position)) {
                                        // Else, if the item is already in the array, remove it
                                        selectedDays.remove(Integer.valueOf(position));
                                    }
                                }
                            })
                            // Set the action buttons
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            onOkay(selectedDays);
                        }
                    })
                    .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            //Log.dismiss();
                        }
                    });

            return repeatDialog.create();
        }

        public void onOkay(ArrayList<Integer> selectedItems) {
            String[] daysArray = getResources().getStringArray(R.array.days);
            // Do something with the time chosen by the user
            Button timeButton = (Button) getActivity().findViewById(R.id.repeat);

            if (selectedItems.size() == 0) {
                repeatStringBuilder = repeatStringBuilder.append("No repeat");
            } else {
                if (selectedItems.size() < daysArray.length) {
                    for (int i = 0; i < selectedItems.size(); i++) {
                        String days = daysArray[selectedItems.get(i)];
                        repeatStringBuilder = repeatStringBuilder.append(days).append(", ");
                    }
                } else if (selectedItems.size() == daysArray.length) {
                    repeatStringBuilder = repeatStringBuilder.append("Everyday");
                }
            }
            timeButton.setText(repeatStringBuilder.toString());
        }
    }
}
