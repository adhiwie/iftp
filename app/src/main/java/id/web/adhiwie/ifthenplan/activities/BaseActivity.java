package id.web.adhiwie.ifthenplan.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;

import id.web.adhiwie.ifthenplan.R;

public abstract class BaseActivity extends AppCompatActivity {
    protected Toolbar toolbar;

    protected abstract int getLayoutResId();

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResId());

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setTitle(getTitle());
    }
}
