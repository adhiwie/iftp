package id.web.adhiwie.ifthenplan.activities;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.estimote.sdk.Beacon;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.wooplr.spotlight.SpotlightView;

import java.util.ArrayList;
import java.util.Calendar;

import id.web.adhiwie.ifthenplan.R;
import id.web.adhiwie.ifthenplan.adapter.CueAdapter;
import id.web.adhiwie.ifthenplan.model.CueModel;
import id.web.adhiwie.ifthenplan.model.GeofenceModel;
import id.web.adhiwie.ifthenplan.model.GoalModel;
import id.web.adhiwie.ifthenplan.service.AlarmIntentService;
import id.web.adhiwie.ifthenplan.service.TriggerService;
import id.web.adhiwie.ifthenplan.util.SharedPreferenceForBeacon;
import id.web.adhiwie.ifthenplan.util.SharedPreferenceForGeofence;
import id.web.adhiwie.ifthenplan.util.SharedPreferenceForTempBeacon;
import id.web.adhiwie.ifthenplan.util.SharedPreferenceForCues;

public class CreateGoalActivity extends BaseActivity {

    private static final String TAG = CreateGoalActivity.class.getSimpleName();

    private Context context;

    private boolean beacon = false;
    private boolean location = false;
    private boolean time = false;

    private SharedPreferenceForCues sharedPreferenceForCues;
    private SharedPreferenceForGeofence sharedPreferenceForGeofence;
    private SharedPreferenceForTempBeacon sharedPreferenceForTempBeacon;
    private SharedPreferenceForBeacon sharedPreferenceForBeacon;

    private ArrayList<CueModel> cues;
    private ArrayList<Beacon> beacons;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_create_goal;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                startActivity(new Intent(CreateGoalActivity.this, MainActivity.class));;
            }
        });

        context = getApplicationContext();
        sharedPreferenceForCues = new SharedPreferenceForCues();
        sharedPreferenceForGeofence = new SharedPreferenceForGeofence();
        sharedPreferenceForTempBeacon = new SharedPreferenceForTempBeacon();
        sharedPreferenceForBeacon = new SharedPreferenceForBeacon();
        cues = new ArrayList<>();

        initCuesList();

        CueAdapter mAdapter = new CueAdapter(context, cues, TAG);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.cues_list);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(true);
        recyclerView.setHasFixedSize(false);
        recyclerView.setAdapter(mAdapter);

        //showTutorial("Confirm", "Once everything is set, press Confirm", findViewById(R.id.confirm));
        //showTutorial("Goal","Enter the goal that will be activated when the cue is detected", findViewById(R.id.goal_title));
        showTutorial("Cues","Set the IF situation cues that will trigger your THEN goal. You can use beacon, locations, and times as IF cues.", findViewById(R.id.add_cues));
        showTutorial("Goal","Set the THEN goal - something specific, concrete and positive that you will do in your IF situation.", findViewById(R.id.goal_title));

    }

    public void addCues(View view){
        Intent intent = new Intent(this, AddCuesActivity.class);
        startActivity(intent);
    }

    protected void initCuesList(){
        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            if(bundle.getStringArrayList("beacon") != null){
                saveTempCue(bundle.getStringArrayList("beacon"));
                saveTempBeacon();
                beacon = true;
            } else if(bundle.getStringArrayList("location") != null){
                saveTempCue(bundle.getStringArrayList("location"));
                location = true;
            } else if(bundle.getStringArrayList("time") != null){
                saveTempCue(bundle.getStringArrayList("time"));
                time = true;
            }
            cues = sharedPreferenceForCues.loadCues(context);
            beacons = sharedPreferenceForTempBeacon.loadTempBeacon(context);

        }

    }

    protected void saveTempCue(ArrayList<String> tempCue){
        CueModel cue = new CueModel();
        cue.setType(tempCue.get(0));
        cue.setValue(tempCue.get(1));
        sharedPreferenceForCues.addCues(context, cue);
    }

    protected void clearTempCues(){
        sharedPreferenceForCues.clearCues(context);
    }

    private void saveTempBeacon(){
        Beacon beaconItem = getIntent().getParcelableExtra(ListBeaconsActivity.EXTRAS_BEACON);
        sharedPreferenceForTempBeacon.addTempBeacon(context, beaconItem);
    }

    private void saveMapGeofence(GeofenceModel geofence, String goalName){
        sharedPreferenceForGeofence.addGeofence(context, geofence, goalName);
    }

    private void setAlarmService(String goalName){
        ArrayList<Integer> selectedDays = getIntent().getIntegerArrayListExtra("selectedDays");
        int hour = getIntent().getIntExtra("hour", 0);
        int min = getIntent().getIntExtra("min", 0);
        /*
        Intent intent = new Intent(CreateGoalActivity.this, AlarmIntentService.class);
        intent.putExtra("hour", hour);
        intent.putExtra("min", min);
        intent.putIntegerArrayListExtra("selectedDays", selectedDays);
        intent.putExtra("goalName", goalName);
        startService(intent);
        */
        for (int selectedDay : selectedDays) {
            Log.d(TAG, Integer.toString(selectedDay));

            Calendar cal = Calendar.getInstance();

            cal.set(Calendar.DAY_OF_WEEK, ++selectedDay);
            cal.set(Calendar.HOUR_OF_DAY, hour);
            cal.set(Calendar.MINUTE, min);

            Intent in = new Intent(this, TriggerService.class);
            in.putExtra("time", true);

            //PendingIntent pi = PendingIntent.getBroadcast(this, selectedDay, in, Intent.FILL_IN_DATA | PendingIntent.FLAG_CANCEL_CURRENT);
            PendingIntent pi = PendingIntent.getService(this, selectedDay, in, Intent.FILL_IN_DATA | PendingIntent.FLAG_CANCEL_CURRENT);
            AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            am.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pi);
        }
    }

    private void showTutorial(String title, String message, View view){
        new SpotlightView.Builder(this)
                .introAnimationDuration(400)
                .performClick(false)
                .fadeinTextDuration(400)
                .headingTvColor(Color.parseColor("#eb273f"))
                .headingTvSize(32)
                .headingTvText(title)
                .subHeadingTvColor(Color.parseColor("#ffffff"))
                .subHeadingTvSize(16)
                .subHeadingTvText(message)
                .maskColor(Color.parseColor("#dc000000"))
                .target(view)
                .lineAnimDuration(400)
                .lineAndArcColor(Color.parseColor("#eb273f"))
                .dismissOnTouch(true)
                .dismissOnBackPress(true)
                .enableDismissAfterShown(true)
                .show();
    }

    private void sendParcel(String goalName, ArrayList<CueModel> cues){
        GoalModel goal = new GoalModel();
        goal.setGoalName(goalName);
        goal.setCues(cues);
        saveGoaltoFirebase(goal);
        Intent intent = new Intent(this, RehearsalOneActivity.class);
        intent.putExtra("goal", goal);
        sharedPreferenceForTempBeacon.clearTempBeacon(context);
        sharedPreferenceForBeacon.storeBeacon(context, beacons);

        startActivity(intent);
        /*
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("goal", goal);
        intent.putParcelableArrayListExtra("beacon", sharedPreferenceForTempBeacon.loadBeacon(context));
        sharedPreferenceForTempBeacon.clearBeacon(context);
        startActivity(intent);
        */
    }

    public void saveGoal(View view){
        EditText etGoalName = (EditText) findViewById(R.id.goal_title);
        String goalName = etGoalName.getText().toString();

        if(goalName.equals("")){
            Toast.makeText(this, "Goal name may not be empty", Toast.LENGTH_SHORT).show();
        } else if(cues.size() < 1){
            Toast.makeText(this, "Cues may not be empty", Toast.LENGTH_SHORT).show();
        } else {
            clearTempCues();

            /*
            if(location){
                GeofenceModel geofence = getIntent().getExtras().getParcelable("geofence");
                if(geofence != null){
                    saveMapGeofence(geofence, goalName);
                }
            }

            if(time){
                setAlarmService(goalName);
            }
            */

            sendParcel(goalName, cues);

        }
    }

    private void saveGoaltoFirebase(GoalModel goal){
        SharedPreferences sharedPref = getSharedPreferences("uid", 0);
        String uid = sharedPref.getString("uid", "@null");
        DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference();
        dbRef.child("users").child(uid).child("goals").push().setValue(goal);
    }
}