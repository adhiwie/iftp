package id.web.adhiwie.ifthenplan.activities;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Toast;

import com.chyrta.onboarder.OnboarderActivity;
import com.chyrta.onboarder.OnboarderPage;

import java.util.ArrayList;
import java.util.List;

import id.web.adhiwie.ifthenplan.R;

/**
 * Created by adhiwie on 26/10/2016.
 */

public class IntroActivity extends OnboarderActivity {

    List<OnboarderPage> onboarderPages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences sharedPref = getSharedPreferences("uid", 0);
        if(sharedPref.contains("uid")){
            Intent intent = new Intent(IntroActivity.this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            this.finish();
        }

        onboarderPages = new ArrayList<>();

        // Create your first page
        String intro = "This is a study into making effective IF THEN plans to help people change their behaviour.\n\n" +
                "Research has shown that planning is more effective if you first identify a particular situation (IF), and decide on a goal for what you will do in that situation (THEN).\n\n"
        + "For example “IF I go towards the lift, THEN I will take the stairs”.\n\n" +
                "To support you to make some IF THEN plans, we have placed IF Bluetooth triggers in the following locations: by the front door, lifts, and kitchens.\n\n";
        String intro2 = "Please take a moment to think about what plan(s) you want to make. For the IF, think about a particular location's cues - you can use the Bluetooth beacons, time and broader location as location cues.\n\n" +
                "For the THEN, think about a relatively specific, concrete, positive goal (i.e. what you would specifically like to do in the given situation, rather than something you'd like to stop doing).\n\n" +
                "Then follow the instructions to combine location cues and your goals to form your new IF THEN plans!";
        OnboarderPage onboarderPage1 = new OnboarderPage("About this study", intro);
        OnboarderPage onboarderPageIntro2 = new OnboarderPage("Making IF THEN plans", intro2);

        OnboarderPage onboarderPage2 = new OnboarderPage("Welcome", "Before we start, please enable Bluetooth and location service permissions.");

        // Don't forget to set background color for your page
        onboarderPage1.setBackgroundColor(R.color.colorPrimary);
        onboarderPage2.setBackgroundColor(R.color.colorPrimary);
        onboarderPageIntro2.setBackgroundColor(R.color.colorPrimary);

        // Add your pages to the list
        onboarderPages.add(onboarderPage2);
        onboarderPages.add(onboarderPage1);
        onboarderPages.add(onboarderPageIntro2);


        setDividerVisibility(View.GONE);
        setSkipButtonHidden();
        shouldUseFloatingActionButton(true);

        // And pass your pages to 'setOnboardPagesReady' method
        setOnboardPagesReady(onboarderPages);



    }

    @Override
    public void onSkipButtonPressed() {
        // Optional: by default it skips onboarder to the end
        super.onSkipButtonPressed();
        // Define your actions when the user press 'Skip' button
    }


    @Override
    public void onFinishButtonPressed() {
        if(!enableBluetooth()){
            Toast.makeText(this, "Bluetooth is not enabled", Toast.LENGTH_LONG).show();
        } else {
            enableGPS();
        }
    }

    private boolean enableBluetooth(){
        boolean res = false;
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            res = false;
        } else {
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, 1234);
            } else {
                res = true;
            }
        }

        return res;
    }

    private void enableGPS(){
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch(Exception ex) {}

        try {
            network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch(Exception ex) {}

        if(!gps_enabled && !network_enabled){
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                            startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                            dialog.cancel();
                        }
                    });
            final AlertDialog alert = builder.create();
            alert.show();
        } else {
            startActivity(new Intent(IntroActivity.this, SignupActivity.class));
        }
    }
}
