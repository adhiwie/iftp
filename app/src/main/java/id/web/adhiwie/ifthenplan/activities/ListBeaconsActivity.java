package id.web.adhiwie.ifthenplan.activities;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.MacAddress;
import com.estimote.sdk.Region;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import id.web.adhiwie.ifthenplan.R;
import id.web.adhiwie.ifthenplan.adapter.BeaconListAdapter;

/**
 * Displays list of found beacons sorted by RSSI.
 * Starts new activity with selected beacon if activity was provided.
 *
 * @author wiktor.gworek@estimote.com (Wiktor Gworek)
 */
public class ListBeaconsActivity extends BaseActivity {
  private static final String TAG = ListBeaconsActivity.class.getSimpleName();

  public static final String EXTRAS_TARGET_ACTIVITY = "CreateActivity";
  public static final String EXTRAS_BEACON = "extrasBeacon";

  private static final int REQUEST_ENABLE_BT = 1234;
  private static final Region ALL_ESTIMOTE_BEACONS_REGION = new Region("rid", null, null, null);

  private BeaconManager beaconManager;
  private BeaconListAdapter adapter;
  private List<Beacon> beacons;

  @Override protected int getLayoutResId() {
    return R.layout.list_beacon;
  }

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    toolbar.setNavigationOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        startActivity(new Intent(ListBeaconsActivity.this, CreateGoalActivity.class));;
      }
    });

    // Configure device list.
    adapter = new BeaconListAdapter(this);
    ListView list = (ListView) findViewById(R.id.device_list);
    list.setAdapter(adapter);
    list.setOnItemClickListener(createOnItemClickListener());

    beacons = new ArrayList<>();

    Beacon beacon1 = new  Beacon(UUID.fromString("B9407F30-F5F8-466E-AFF9-25556B57FE6D"),
                          MacAddress.fromString("f015:ec:b1:e7:ea"), 59370, 60593, -81, -63);
    beacons.add(beacon1);

    Beacon beacon2 = new  Beacon(UUID.fromString("B9407F30-F5F8-466E-AFF9-25556B57FE6D"),
            MacAddress.fromString("d1d8632da745"), 42821, 25389, -81, -63);
    beacons.add(beacon2);

    Beacon beacon3 = new  Beacon(UUID.fromString("B9407F30-F5F8-466E-AFF9-25556B57FE6D"),
            MacAddress.fromString("cf20146e899c"), 35228, 5230, -81, -63);
    beacons.add(beacon3);

    adapter.replaceWith(beacons);



    /*
    // Configure BeaconManager.
    beaconManager = new BeaconManager(this);
    beaconManager.setRangingListener(new BeaconManager.RangingListener() {
      @Override public void onBeaconsDiscovered(Region region, final List<Beacon> beacons) {
        // Note that results are not delivered on UI thread.
        runOnUiThread(new Runnable() {
          @Override public void run() {
            // Note that beacons reported here are already sorted by estimated
            // distance between device and beacon.
            toolbar.setSubtitle("Found beacons: " + beacons.size());
            adapter.replaceWith(beacons);
          }
        });
      }
    });
    */
  }

  /*

  @Override protected void onDestroy() {
    beaconManager.disconnect();

    super.onDestroy();
  }

  @Override protected void onStart() {
    super.onStart();

    BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    if (mBluetoothAdapter == null) {
      Toast.makeText(this, "Device does not have Bluetooth Low Energy", Toast.LENGTH_LONG).show();
    } else {
      if (!mBluetoothAdapter.isEnabled()) {
        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
      } else {
        connectToService();
      }
    }

  }

  @Override protected void onStop() {
    beaconManager.stopRanging(ALL_ESTIMOTE_BEACONS_REGION);

    super.onStop();
  }

  @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (requestCode == REQUEST_ENABLE_BT) {
      if (resultCode == Activity.RESULT_OK) {
        connectToService();
      } else {
        Toast.makeText(this, "Bluetooth not enabled", Toast.LENGTH_LONG).show();
        toolbar.setSubtitle("Bluetooth not enabled");
      }
    }
    super.onActivityResult(requestCode, resultCode, data);
  }

  private void connectToService() {
    toolbar.setSubtitle("Scanning...");
    adapter.replaceWith(Collections.<Beacon>emptyList());
    beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
      @Override public void onServiceReady() {
        beaconManager.startRanging(ALL_ESTIMOTE_BEACONS_REGION);
      }
    });
  }
*/
  private AdapterView.OnItemClickListener createOnItemClickListener() {
    return new AdapterView.OnItemClickListener() {
      @Override public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (getIntent().getStringExtra(EXTRAS_TARGET_ACTIVITY) != null) {
          try {
            Class<?> clazz = Class.forName(getIntent().getStringExtra(EXTRAS_TARGET_ACTIVITY));
            Intent intent = new Intent(ListBeaconsActivity.this, clazz);
            intent.putExtra(EXTRAS_BEACON, adapter.getItem(position));

            String beaconName;
            if(adapter.getItem(position).getMajor() == 42821){
              beaconName = "Kitchen";
            }else if(adapter.getItem(position).getMajor() == 59370){
              beaconName = "Front door";
            }else if(adapter.getItem(position).getMajor() == 35228){
              beaconName = "Lift";
            }else{
              beaconName = "N/A";
            }
            ArrayList<String> beacon = new ArrayList<>();
            beacon.add("Beacon");
            beacon.add(beaconName);
            intent.putExtra("beacon", beacon);
            startActivity(intent);
          } catch (ClassNotFoundException e) {
            Log.e(TAG, "Finding class by name failed", e);
          }
        }
      }
    };
  }
}
