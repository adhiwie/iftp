package id.web.adhiwie.ifthenplan.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import id.web.adhiwie.ifthenplan.R;
import id.web.adhiwie.ifthenplan.adapter.GoalAdapter;
import id.web.adhiwie.ifthenplan.model.GoalModel;
import id.web.adhiwie.ifthenplan.service.GeofenceIntentService;
import id.web.adhiwie.ifthenplan.service.TriggerService;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private String uid;
    private ArrayList<GoalModel> goals;
    private DatabaseReference dbRef;
    private GoalAdapter goalAdapter;

    private ArrayList<Beacon> beacons;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Context context = getApplicationContext();
        goals = new ArrayList<>();
        beacons = new ArrayList<>();

        dbRef = FirebaseDatabase.getInstance().getReference();

        SharedPreferences sharedPref = getSharedPreferences("uid", 0);
        uid = sharedPref.getString("uid", "@null");

        goalAdapter = new GoalAdapter(context, goals, TAG);

        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            GoalModel goal = bundle.getParcelable("goal");
            beacons = bundle.getParcelableArrayList("beacon");
            //saveGoaltoFirebase(goal);
            bundle.clear();
        }
        if(beacons != null){
            startBeaconMonitoring(beacons);
        }

        loadGoalFromFirebase();
        startGeofenceIntentService();
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    private void saveGoaltoFirebase(GoalModel goal){
        dbRef.child("users").child(uid).child("goals").push().setValue(goal);
    }

    private void loadGoalFromFirebase(){
        dbRef.child("users").child(uid).child("goals").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot goalSnapshop : dataSnapshot.getChildren()){
                    GoalModel goal = goalSnapshop.getValue(GoalModel.class);
                    goal.setKey(goalSnapshop.getKey());
                    goals.add(goal);
                }
                loadGoalToRecyclerView();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void loadGoalToRecyclerView(){
        RecyclerView goalRecyclerView = (RecyclerView) findViewById(R.id.goal_list);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        goalRecyclerView.setLayoutManager(mLayoutManager);
        goalRecyclerView.setItemAnimator(new DefaultItemAnimator());
        goalRecyclerView.setNestedScrollingEnabled(true);
        goalRecyclerView.setHasFixedSize(false);
        goalRecyclerView.setAdapter(goalAdapter);

        if(goals.size() > 0){
            TextView  empty = (TextView) findViewById(R.id.empty_message);
            empty.setVisibility(View.GONE);
        }
    }


    public void addGoal(View view) {
        Intent intent = new Intent(this, CreateGoalActivity.class);
        startActivity(intent);
    }

    protected void startGeofenceIntentService() {
        Intent intent = new Intent(this, GeofenceIntentService.class);
        startService(intent);
    }

    protected void startBeaconMonitoring(ArrayList<Beacon> beacons){
        final BeaconManager beaconManager = new BeaconManager(this);

        for (Beacon beacon : beacons){
            final Region region = new Region("regionId", beacon.getProximityUUID(), beacon.getMajor(), beacon.getMinor());

            beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
                @Override
                public void onServiceReady() {
                    beaconManager.startMonitoring(region);
                }
            });

            beaconManager.setMonitoringListener(new BeaconManager.MonitoringListener() {
                @Override
                public void onEnteredRegion(Region region, List<Beacon> beacons) {
                    //postNotification(goalName);
                    Intent intent = new Intent(MainActivity.this, TriggerService.class);
                    intent.putExtra("beacon", true);
                    startService(intent);
                }

                @Override
                public void onExitedRegion(Region region) {
                }

            });
        }
    }
}
