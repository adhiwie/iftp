package id.web.adhiwie.ifthenplan.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import id.web.adhiwie.ifthenplan.R;

/**
 * Created by adhiwie on 01/11/2016.
 */

public class NotifyActivity extends AppCompatActivity {

    private static final String TAG = NotifyActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notify);
    }
}
