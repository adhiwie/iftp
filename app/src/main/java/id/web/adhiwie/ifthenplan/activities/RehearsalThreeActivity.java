package id.web.adhiwie.ifthenplan.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

import id.web.adhiwie.ifthenplan.R;
import id.web.adhiwie.ifthenplan.model.CueModel;
import id.web.adhiwie.ifthenplan.model.GoalModel;

public class RehearsalThreeActivity extends AppCompatActivity {

    private GoalModel goal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rehearsal_three);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            goal = bundle.getParcelable("goal");
            initGoal(goal);
            bundle.clear();
        }

        //confirm button clicked
        View confirmButton = this.findViewById(R.id.confirm);

        if (confirmButton != null) {
            confirmButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.putExtra("goal", goal);
                    startActivity(intent);
                }
            });
        }
    }

    private void initGoal(GoalModel goal) {
        String message = "Now, imagine you have set a goal : " +
                "<b>" + goal.getGoalName() + "</b>. " +
                "What is the situation where you will perform this goal?";

        TextView msg = (TextView) findViewById(R.id.message);
        msg.setText(Html.fromHtml(message));

    }
}
