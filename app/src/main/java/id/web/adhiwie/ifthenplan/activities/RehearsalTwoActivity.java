package id.web.adhiwie.ifthenplan.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

import id.web.adhiwie.ifthenplan.R;
import id.web.adhiwie.ifthenplan.model.CueModel;
import id.web.adhiwie.ifthenplan.model.GoalModel;

public class RehearsalTwoActivity extends AppCompatActivity {

    private StringBuilder stringBuilder;
    private GoalModel goal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rehearsal_two);

        stringBuilder = new StringBuilder();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            goal = bundle.getParcelable("goal");
            initGoal(goal);
            bundle.clear();
        }

        //confirm button clicked
        View confirmButton = this.findViewById(R.id.confirm);

        if (confirmButton != null) {
            confirmButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getApplicationContext(), RehearsalThreeActivity.class);
                    intent.putExtra("goal", goal);
                    startActivity(intent);
                }
            });
        }
    }

    private void initGoal(GoalModel goal) {
        String goalName = goal.getGoalName();
        ArrayList<CueModel> cues = goal.getCues();
        for (CueModel cue : cues) {
            switch (cue.getType()) {
                case "Location":
                    stringBuilder.append("you are near " + cue.getValue() + ", ");
                    break;
                case "Beacon":
                    stringBuilder.append("you are approaching " + cue.getValue() + ", ");
                    break;
                case "Time":
                    stringBuilder.append("the time is " + cue.getValue() + ", ");
                    break;

            }
        }

        String message = "Now, when this situation happens: " +
                "<b>" + stringBuilder.toString() + "</b> " +
                " what are you going to do?";

        TextView msg = (TextView) findViewById(R.id.message);
        msg.setText(Html.fromHtml(message));

    }
}
