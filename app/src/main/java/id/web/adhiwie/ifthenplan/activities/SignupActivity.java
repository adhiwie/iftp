package id.web.adhiwie.ifthenplan.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import id.web.adhiwie.ifthenplan.R;
import id.web.adhiwie.ifthenplan.model.UserModel;

public class SignupActivity extends AppCompatActivity {

    private SharedPreferences sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        sharedPref = getSharedPreferences("uid", 0);
        if(sharedPref.contains("uid")){
            getStarted();
        }
    }

    public void startExperiment(View view){
        EditText etExperimentId = (EditText) findViewById(R.id.experiment_id);
        String experimentId = etExperimentId.getText().toString();
        if(experimentId.trim().length() == 0) {
            Snackbar snackbar;
            snackbar = Snackbar.make(view, "Experiment ID may not be empty", Snackbar.LENGTH_LONG);
            snackbar.getView().setBackgroundColor(Color.RED);
            snackbar.show();
        } else {
            new WriteUid().execute(experimentId);
        }
    }

    public void getStarted(){
        Intent intent = new Intent(SignupActivity.this, CreateGoalActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        this.finish();
    }

    private class WriteUid extends AsyncTask<String, Void, Void>{

        @Override
        protected Void doInBackground(String... experimentId) {
            UserModel userModel = new UserModel();
            userModel.setExperimentId(experimentId[0]);

            DatabaseReference ref = FirebaseDatabase.getInstance().getReference("users");
            String uid = ref.push().getKey();

            ref.child(uid).setValue(userModel);

            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString("uid", uid);
            editor.apply();

            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            getStarted();
        }
    }
}
