package id.web.adhiwie.ifthenplan.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

import id.web.adhiwie.ifthenplan.R;

/**
 * Created by adhiwie on 02/11/2016.
 */

public class UpdateGoalActivity extends BaseActivity {

    private EditText goalNameEditText;
    private String goalId, goalName, uid;
    private DatabaseReference dbRef;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_update_goal;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                startActivity(new Intent(UpdateGoalActivity.this, MainActivity.class));;
            }
        });


        goalNameEditText = (EditText) findViewById(R.id.goal_name);
        goalId = getIntent().getStringExtra("goalId");
        goalName = getIntent().getStringExtra("goalName");
        goalNameEditText.setText(goalName);
        dbRef = FirebaseDatabase.getInstance().getReference();
        SharedPreferences sharedPref = getSharedPreferences("uid", 0);
        uid = sharedPref.getString("uid", "@null");

        //confirm button clicked
        View addButton = this.findViewById(R.id.confirm);

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Query query = dbRef.child("users").child(uid).child("goals").equalTo(goalId);
                query.addListenerForSingleValueEvent(new ValueEventListener() {

                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        HashMap<String, Object> result = new HashMap<>();
                        result.put("goalName",goalNameEditText.getText().toString());
                        dbRef.child("users").child(uid).child("goals").child(goalId).updateChildren(result);

                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);

                        Toast.makeText(getApplicationContext(), "Goal has been updated", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        });

    }

}
