package id.web.adhiwie.ifthenplan.adapter;

/**
 * Created by adhiwie on 12/10/2016.
 */

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import id.web.adhiwie.ifthenplan.R;
import id.web.adhiwie.ifthenplan.activities.UpdateCueActivity;
import id.web.adhiwie.ifthenplan.model.CueModel;
import id.web.adhiwie.ifthenplan.util.SharedPreferenceForCues;

public class CueAdapter extends RecyclerView.Adapter<CueAdapter.ViewHolder> {

    private List<CueModel> cues;
    private Context context;
    private String className;

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView title, description;
        ImageButton removeIcon;
        ImageView cueIcon;
        LinearLayout cueLayout;


        ViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            description = (TextView) view.findViewById(R.id.description);
            removeIcon = (ImageButton) view.findViewById(R.id.remove);
            cueIcon = (ImageView) view.findViewById(R.id.cue_icon);
            cueLayout = (LinearLayout) view.findViewById(R.id.cue_layout);
        }
    }

    public CueAdapter(Context context, List<CueModel> cues, String className) {
        this.cues = cues;
        this.context = context;
        this.className = className;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = null;
        if(className.equals("MainActivity")){
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.adapter_cues_list_fixed, parent, false);
        } else if(className.equals("CreateGoalActivity")){
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.adapter_cues_list, parent, false);
        }

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final CueModel cue = cues.get(position);
        GradientDrawable background = (GradientDrawable) holder.cueLayout.getBackground();

        switch (cue.getType()) {
            case "Location":
                background.setColor(ContextCompat.getColor(context, R.color.cue1));
                holder.cueIcon.setBackgroundResource(R.drawable.ic_location);
                break;
            case "Beacon":
                background.setColor(ContextCompat.getColor(context, R.color.cue2));
                holder.cueIcon.setBackgroundResource(R.drawable.ic_beacon);
                break;
            case "Time":
                background.setColor(ContextCompat.getColor(context, R.color.cue3));
                holder.cueIcon.setBackgroundResource(R.drawable.ic_time);
                break;
        }

        holder.title.setText(cue.getType());
        holder.description.setText(cue.getValue());
        if(className.equals("CreateGoalActivity")){
            holder.removeIcon.setOnClickListener(new View.OnClickListener(){

                @Override
                public void onClick(View view) {
                    SharedPreferenceForCues sharedPreferenceForCues = new SharedPreferenceForCues();
                    sharedPreferenceForCues.removeCues(context, cue);
                    cues.remove(cue);
                    notifyItemRemoved(position);
                    notifyItemRangeChanged(position, cues.size());
                }
            });
        } else if(className.equals("MainActivity")){
            holder.cueLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, UpdateCueActivity.class);
                    intent.putExtra("cueName", cue.getType());
                    intent.putExtra("cueDesc", cue.getValue());
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return cues.size();
    }
}
