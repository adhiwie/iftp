package id.web.adhiwie.ifthenplan.adapter;

/**
 * Created by adhiwie on 12/10/2016.
 */

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.List;

import id.web.adhiwie.ifthenplan.R;
import id.web.adhiwie.ifthenplan.activities.UpdateGoalActivity;
import id.web.adhiwie.ifthenplan.model.GoalModel;

public class GoalAdapter extends Adapter<GoalAdapter.ViewHolder> {

    private List<GoalModel> goals;
    private Context context;
    private String className;

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView goalName;
        RecyclerView cueRecycleView;

        ViewHolder(View view) {
            super(view);
            goalName = (TextView) view.findViewById(R.id.goal_name);
            cueRecycleView = (RecyclerView) view.findViewById(R.id.cues_list);
        }
    }

    public GoalAdapter(Context context, List<GoalModel> goals, String className) {
        this.goals = goals;
        this.context = context;
        this.className = className;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_goal_list, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final GoalModel goal = goals.get(position);
        holder.goalName.setText(goal.getGoalName());
        CueAdapter cueAdapter = new CueAdapter(context, goal.getCues(), className);
        holder.cueRecycleView.setAdapter(cueAdapter);
        holder.cueRecycleView.setHasFixedSize(false);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        holder.cueRecycleView.setLayoutManager(new GridLayoutManager(context, 2));

        holder.goalName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, UpdateGoalActivity.class);
                intent.putExtra("goalId", goal.getKey());
                intent.putExtra("goalName", goal.getGoalName());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return goals.size();
    }
}
