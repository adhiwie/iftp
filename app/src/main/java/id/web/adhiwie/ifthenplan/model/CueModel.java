package id.web.adhiwie.ifthenplan.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by adhiwie on 23/09/16.
 */
public class CueModel implements Parcelable {

    private String type;
    private String value;

    public void setType(String type) {
        this.type = type;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getType() {

        return type;
    }

    public String getValue() {
        return value;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(type);
        parcel.writeString(value);
    }

    public static final Creator CREATOR = new Creator() {
        public CueModel createFromParcel(Parcel parcel) {
            return new CueModel(parcel);
        }

        public CueModel[] newArray(int size) {
            return new CueModel[size];
        }
    };

    private CueModel(Parcel parcel) {
        type = parcel.readString();
        value = parcel.readString();
    }

    public CueModel(){}
}
