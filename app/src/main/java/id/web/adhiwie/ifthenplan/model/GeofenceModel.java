package id.web.adhiwie.ifthenplan.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by adhiwie on 01/03/15.
 */
public class GeofenceModel implements Parcelable {
    private String key;
    private String goalName;
    private String place;
    private Double latitude;
    private Double longitude;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getGoalName() {
        return goalName;
    }

    public void setGoalName(String goalName) {
        this.goalName = goalName;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(key);
        parcel.writeString(place);
        parcel.writeDouble(latitude);
        parcel.writeDouble(longitude);
    }

    public static final Creator CREATOR = new Creator() {
        public GeofenceModel createFromParcel(Parcel parcel) {
            return new GeofenceModel(parcel);
        }

        public GeofenceModel[] newArray(int size) {
            return new GeofenceModel[size];
        }
    };

    private GeofenceModel(Parcel parcel) {
        key = parcel.readString();
        place = parcel.readString();
        latitude = parcel.readDouble();
        longitude = parcel.readDouble();
    }

    public GeofenceModel(){}

}
