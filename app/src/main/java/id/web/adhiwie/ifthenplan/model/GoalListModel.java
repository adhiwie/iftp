package id.web.adhiwie.ifthenplan.model;

import java.util.ArrayList;

public class GoalListModel {
    private String key;
    private String goal;
    private ArrayList<String> cues;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getGoal() {
        return goal;
    }

    public void setGoal(String goal) {
        this.goal = goal;
    }

    public ArrayList<String> getCues() {
        return cues;
    }

    public void setCues(ArrayList<String> cues) {
        this.cues = cues;
    }
}
