package id.web.adhiwie.ifthenplan.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by adhiwie on 23/09/16.
 */
public class GoalModel implements Parcelable {

    private String key;
    private String goalName;
    private ArrayList<CueModel> cues;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setGoalName(String goalName) {
        this.goalName = goalName;
    }

    public void setCues(ArrayList<CueModel> cues) {
        this.cues = cues;
    }

    public String getGoalName() {
        return goalName;
    }

    public ArrayList<CueModel> getCues() {
        return cues;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(key);
        parcel.writeString(goalName);
        parcel.writeTypedList(cues);
    }

    public static final Creator CREATOR = new Creator() {
        public GoalModel createFromParcel(Parcel parcel) {
            return new GoalModel(parcel);
        }

        public GoalModel[] newArray(int size) {
            return new GoalModel[size];
        }
    };

    private GoalModel(Parcel parcel) {
        key = parcel.readString();
        goalName = parcel.readString();
        cues = new ArrayList<>();
        parcel.readTypedList(cues, CueModel.CREATOR);
    }

    public GoalModel(){}
}
