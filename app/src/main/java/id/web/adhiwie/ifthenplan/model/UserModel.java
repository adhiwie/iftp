package id.web.adhiwie.ifthenplan.model;

/**
 * Created by adhiwie on 23/09/16.
 */
public class UserModel {

    private String experimentId;

    public UserModel(){}

    public String getExperimentId() {
        return experimentId;
    }

    public void setExperimentId(String experimentId) {
        this.experimentId = experimentId;
    }
}
