package id.web.adhiwie.ifthenplan.service;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by adhiwie on 31/08/15.
 */
public class AlarmIntentService extends IntentService {

    private static final String TAG = AlarmIntentService.class.getSimpleName();

    public AlarmIntentService(){
        super("AlarmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        ArrayList<Integer> selectedDays = intent.getIntegerArrayListExtra("selectedDays");
        int hour = intent.getIntExtra("hour", 0);
        int min = intent.getIntExtra("min", 0);
        String goalName = intent.getStringExtra("goalName");
        for (int selectedDay : selectedDays) {

            Calendar cal = Calendar.getInstance();

            cal.set(Calendar.DAY_OF_WEEK, ++selectedDay);
            cal.set(Calendar.HOUR_OF_DAY, hour);
            cal.set(Calendar.MINUTE, min);

            Intent in = new Intent(this, TriggerService.class);
            in.putExtra("time", true);

            //PendingIntent pi = PendingIntent.getBroadcast(this, selectedDay, in, Intent.FILL_IN_DATA | PendingIntent.FLAG_CANCEL_CURRENT);
            PendingIntent pi = PendingIntent.getService(this, selectedDay, in, 0);
            AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            am.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pi);
        }
    }
}
