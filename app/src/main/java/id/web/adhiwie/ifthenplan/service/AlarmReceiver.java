package id.web.adhiwie.ifthenplan.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import id.web.adhiwie.ifthenplan.R;
import id.web.adhiwie.ifthenplan.activities.MainActivity;

/**
 * Created by adhiwie on 31/08/15.
 */
public class AlarmReceiver extends BroadcastReceiver {

    private static final String TAG = AlarmReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        String goalName = intent.getStringExtra("goal");
        NotificationManager notificationManager;
        notificationManager = (NotificationManager)context.getSystemService(context.NOTIFICATION_SERVICE);

        Intent notifyIntent = new Intent(context, MainActivity.class);
        notifyIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivities(
                context,
                0,
                new Intent[]{notifyIntent},
                PendingIntent.FLAG_UPDATE_CURRENT
        );
        Notification notification = new Notification.Builder(context)
                .setSmallIcon(R.drawable.ic_check_box)
                .setContentTitle("If-then Plans")
                .setContentText(goalName)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .build();
        notification.defaults |= Notification.DEFAULT_SOUND;
        notification.defaults |= Notification.DEFAULT_LIGHTS;
        notification.defaults |= Notification.DEFAULT_VIBRATE;
        notificationManager.notify(0, notification);
    }
}
