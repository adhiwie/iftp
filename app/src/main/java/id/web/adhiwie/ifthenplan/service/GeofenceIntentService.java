package id.web.adhiwie.ifthenplan.service;

import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.List;

import id.web.adhiwie.ifthenplan.model.GeofenceModel;
import id.web.adhiwie.ifthenplan.util.SharedPreferenceForGeofence;


public class GeofenceIntentService extends IntentService
        implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        ResultCallback<Status> {

    public static final String TAG = GeofenceIntentService.class.getSimpleName();

    /** Initialization **/
    private GoogleApiClient mGoogleApiClient;
    private ArrayList<GeofenceModel> geofences;
    private List<Geofence> geofencesList = new ArrayList<>();

    public GeofenceIntentService() {
        super("GeofenceIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        // Building the GoogleApi client
        buildGoogleApiClient();

        SharedPreferenceForGeofence sharedPreferenceForGeofence = new SharedPreferenceForGeofence();
        geofences = sharedPreferenceForGeofence.loadGeofences(getApplicationContext());

        if (!mGoogleApiClient.isConnecting() || !mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
        }
    }

    /**
     * Creating google api client object
     * */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        //Log.d(TAG, "connected!");

        if(geofences != null){
            if (geofences.size() > 0) {
                for (GeofenceModel geofence : geofences) {
                    createGeofences(geofence.getLatitude(),
                            geofence.getLongitude(),
                            geofence.getKey());
                }
            }

            if (geofencesList.size() > 0) {
                createGeofencingRequest(geofencesList);
            }
        }

        //Log.d(TAG, Integer.toString(geofencesList.size()));
    }

    @Override
    public void onConnectionSuspended(int cause) {
        // TODO
        //Log.d(TAG, "connection suspended!");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // TODO
        //Log.d(TAG, "connection failed!");
    }

    public void createGeofences(double latitude, double longitude, String key) {
        // create a Geofence around the location
        Geofence geofence = new Geofence.Builder()
                .setRequestId(key)
                .setExpirationDuration(Geofence.NEVER_EXPIRE)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER)
                .setCircularRegion(latitude, longitude, 50)
                .build();

        geofencesList.add(geofence);
    }

    public void createGeofencingRequest(List<Geofence> geofencesList) {
        GeofencingRequest geofencingRequest = new GeofencingRequest.Builder()
                .addGeofences(geofencesList)
                .setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER)
                .build();

        // Create an Intent pointing to the IntentService
        Intent intent = new Intent(this,
                GeofenceTransitionIntentService.class);
        PendingIntent pi = PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        LocationServices.GeofencingApi.addGeofences(mGoogleApiClient, geofencingRequest, pi);
    }

    @Override
    public void onResult(Status status) {
        if (status.isSuccess()) {
            Toast.makeText(
                    getApplicationContext(),
                    "Geofences Added",
                    Toast.LENGTH_SHORT
            ).show();
        } else {
            Toast.makeText(
                    getApplicationContext(),
                    "Geofences error",
                    Toast.LENGTH_SHORT
            ).show();
        }
    }
}

