package id.web.adhiwie.ifthenplan.service;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Handler;
import android.support.v7.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofenceStatusCodes;
import com.google.android.gms.location.GeofencingEvent;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import id.web.adhiwie.ifthenplan.R;
import id.web.adhiwie.ifthenplan.activities.MainActivity;
import id.web.adhiwie.ifthenplan.activities.NotifyActivity;
import id.web.adhiwie.ifthenplan.model.GeofenceModel;
import id.web.adhiwie.ifthenplan.util.SharedPreferenceForGeofence;

public class GeofenceTransitionIntentService extends IntentService {

    public static final String TAG = GeofenceTransitionIntentService.class.getSimpleName();

    private List<Geofence> geofenceList = new ArrayList<>();
    private String goalName = null;
    private String address = null;
    private Double latitude = null;
    private Double longitude = null;

    public GeofenceTransitionIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        GeofencingEvent event = GeofencingEvent.fromIntent(intent);

        if (event.hasError()) {
            // TODO: Handle error
            Log.e(TAG, "GeofencingEvent Error: " + event.getErrorCode());
        } else {
            int transition = event.getGeofenceTransition();

            if (transition == Geofence.GEOFENCE_TRANSITION_ENTER) {

                geofenceList = event.getTriggeringGeofences();

                for(Geofence geofence : geofenceList){

                    SharedPreferenceForGeofence sharedPreferenceForGeofence = new SharedPreferenceForGeofence();
                    GeofenceModel triggeringGeofence = sharedPreferenceForGeofence.loadGeofenceByKey(getApplicationContext(), geofence.getRequestId());

                    if(triggeringGeofence != null){
                        goalName = triggeringGeofence.getGoalName();
                        address = triggeringGeofence.getPlace();
                        latitude = triggeringGeofence.getLatitude();
                        longitude = triggeringGeofence.getLongitude();
                    }

                    Intent pi = new Intent(GeofenceTransitionIntentService.this, TriggerService.class);
                    pi.putExtra("location", true);
                    startService(pi);
                }

                //Log.d(TAG, "Notified!");
            }
        }
    }

    private void sendNotification() {
        Intent notificationIntent = new Intent(this, MainActivity.class);
        notificationIntent.putExtra("latitude", latitude);
        notificationIntent.putExtra("longitude", longitude);

        PendingIntent contentIntent = PendingIntent.getActivity(this.getApplicationContext(), 0, notificationIntent, 0);

        Notification notification = new Notification.Builder(this.getApplicationContext())
                .setContentTitle(goalName)
                .setContentText("Near "+address)
                .setContentIntent(contentIntent)
                .setSmallIcon(R.drawable.ic_check_box)
                .setLargeIcon(BitmapFactory.decodeResource(this.getResources(), R.drawable.ic_check_box))
                .build();

        //Log.d(TAG, "Notification created");

        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(1, notification);
    }
}
