package id.web.adhiwie.ifthenplan.service;

import android.app.IntentService;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by adhiwie on 03/11/2016.
 */

public class TriggerService extends Service {

    private static final String TAG = TriggerService.class.getSimpleName();

    private boolean beacon;
    private boolean location;
    private boolean time;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(getApplicationContext(), "Service started"+intent.toString(), Toast.LENGTH_LONG).show();
        beacon      = intent.getBooleanExtra("beacon", false);
        location    = intent.getBooleanExtra("location", false);
        time        = intent.getBooleanExtra("time", false);

        if(beacon){
            Log.d(TAG, "entered region");
        }

        if(location){
            Log.d(TAG, "location is entered");
        }

        if(time){
            Log.d(TAG, "time is detected");
        }

        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
