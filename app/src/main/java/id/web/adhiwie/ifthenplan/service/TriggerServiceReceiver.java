package id.web.adhiwie.ifthenplan.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by adhiwie on 03/11/2016.
 */

public class TriggerServiceReceiver extends BroadcastReceiver {

    private static final String TAG = TriggerServiceReceiver.class.getSimpleName();

    private boolean beacon;
    private boolean location;
    private boolean time;


    @Override
    public void onReceive(Context context, Intent intent) {
        beacon = intent.getBooleanExtra("beaconState", false);
        location = intent.getBooleanExtra("locationState", false);
        time = intent.getBooleanExtra("timeState", false);

    }
}
