package id.web.adhiwie.ifthenplan.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.estimote.sdk.Beacon;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by adhiwie on 20/10/2016.
 */

public class SharedPreferenceForBeacon {

    private static final String TAG = SharedPreferenceForBeacon.class.getSimpleName();

    private static final String PREFS_NAME = "beaconPref";
    private static final String BEACON = "beacon";
    public SharedPreferenceForBeacon() {
        super();
    }

    public void storeBeacon(Context context, ArrayList<Beacon> beacons) {
        SharedPreferences sharedPref;
        SharedPreferences.Editor editor;
        sharedPref = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = sharedPref.edit();
        Gson gson = new Gson();
        String jsonFavorites = gson.toJson(beacons);
        editor.putString(BEACON, jsonFavorites);
        editor.apply();
    }
    public ArrayList<Beacon> loadBeacon(Context context) {
        SharedPreferences sharedPref;
        ArrayList<Beacon> beacons;
        sharedPref = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        if (sharedPref.contains(BEACON)) {
            String jsonCues = sharedPref.getString(BEACON, null);
            Gson gson = new Gson();
            beacons = gson.fromJson(jsonCues, new TypeToken<List<Beacon>>(){}.getType());
        } else
            return null;
        return beacons;
    }
    public void addBeacon(Context context, Beacon beacon) {
        ArrayList<Beacon> beacons = loadBeacon(context);
        if (beacons == null) {
            beacons = new ArrayList<>();
        }
        beacons.add(beacon);
        storeBeacon(context, beacons);
    }
    public void removeBeacon(Context context, Beacon beacon) {
        ArrayList<Beacon> beacons = loadBeacon(context);
        if (beacons != null) {
            if(beacons.remove(beacon)){
                Toast.makeText(context, beacon.toString() + " is removed", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(context, "Beacon is removed", Toast.LENGTH_SHORT).show();
            }
            storeBeacon(context, beacons);
        }
    }
    public void clearBeacon(Context context) {
        SharedPreferences sharedPref;
        SharedPreferences.Editor editor;
        sharedPref = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = sharedPref.edit();
        editor.clear();
        editor.apply();
    }
    private void check(Context context) {
        ArrayList beacons = loadBeacon(context);
        Toast.makeText(context, Integer.toString(beacons.size()), Toast.LENGTH_SHORT).show();
    }
}
