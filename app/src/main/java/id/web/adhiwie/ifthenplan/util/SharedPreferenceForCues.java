package id.web.adhiwie.ifthenplan.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import id.web.adhiwie.ifthenplan.model.CueModel;

/**
 * Created by adhiwie on 20/10/2016.
 */

public class SharedPreferenceForCues {

    private static final String TAG = SharedPreferenceForCues.class.getSimpleName();

    private static final String PREFS_NAME = "tempCuesPref";
    private static final String CUES_TEMP = "tempCues";
    public SharedPreferenceForCues() {
        super();
    }

    public void storeCues(Context context, ArrayList<CueModel> cues) {
        SharedPreferences sharedPref;
        SharedPreferences.Editor editor;
        sharedPref = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = sharedPref.edit();
        Gson gson = new Gson();
        String jsonFavorites = gson.toJson(cues);
        editor.putString(CUES_TEMP, jsonFavorites);
        editor.apply();
    }
    public ArrayList<CueModel> loadCues(Context context) {
        SharedPreferences sharedPref;
        ArrayList<CueModel> cues;
        sharedPref = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        if (sharedPref.contains(CUES_TEMP)) {
            String jsonCues = sharedPref.getString(CUES_TEMP, null);
            Gson gson = new Gson();
            cues = gson.fromJson(jsonCues, new TypeToken<List<CueModel>>(){}.getType());
        } else
            return null;
        return cues;
    }
    public void addCues(Context context, CueModel cue) {
        ArrayList<CueModel> cues = loadCues(context);
        if (cues == null) {
            cues = new ArrayList<>();
        }
        cues.add(cue);
        storeCues(context, cues);
    }
    public void removeCues(Context context, CueModel cue) {
        ArrayList<CueModel> cues = loadCues(context);
        if (cues != null) {
            if(cues.remove(cue)){
                Toast.makeText(context, cue.getValue() + " is removed", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(context, "Cue is removed", Toast.LENGTH_SHORT).show();
            }
            storeCues(context, cues);
        }
    }
    public void clearCues(Context context) {
        SharedPreferences sharedPref;
        SharedPreferences.Editor editor;
        sharedPref = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = sharedPref.edit();
        editor.clear();
        editor.apply();
    }
    private void check(Context context) {
        ArrayList cues = loadCues(context);
        Toast.makeText(context, Integer.toString(cues.size()), Toast.LENGTH_SHORT).show();
    }
}
