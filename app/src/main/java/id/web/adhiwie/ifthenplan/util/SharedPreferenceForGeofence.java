package id.web.adhiwie.ifthenplan.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import id.web.adhiwie.ifthenplan.model.CueModel;
import id.web.adhiwie.ifthenplan.model.GeofenceModel;

/**
 * Created by adhiwie on 20/10/2016.
 */

public class SharedPreferenceForGeofence {

    private static final String TAG = SharedPreferenceForGeofence.class.getSimpleName();

    private static final String PREFS_NAME = "geofencePref";
    private static final String CUES_TEMP = "geofence";
    public SharedPreferenceForGeofence() {
        super();
    }

    public void storeGeofences(Context context, ArrayList<GeofenceModel> geofences) {
        SharedPreferences sharedPref;
        SharedPreferences.Editor editor;
        sharedPref = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = sharedPref.edit();
        Gson gson = new Gson();
        String jsonFavorites = gson.toJson(geofences);
        editor.putString(CUES_TEMP, jsonFavorites);
        editor.apply();
    }
    public ArrayList<GeofenceModel> loadGeofences(Context context) {
        SharedPreferences sharedPref;
        ArrayList<GeofenceModel> geofences;
        sharedPref = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        if (sharedPref.contains(CUES_TEMP)) {
            String jsonCues = sharedPref.getString(CUES_TEMP, null);
            Gson gson = new Gson();
            geofences = gson.fromJson(jsonCues, new TypeToken<List<GeofenceModel>>(){}.getType());
        } else
            return null;
        return geofences;
    }
    public GeofenceModel loadGeofenceByKey(Context context, String key){
        GeofenceModel geofence = null;
        for(GeofenceModel geofenceModel : loadGeofences(context)){
            if(geofenceModel.getKey().equals(key)) {
                geofence = geofenceModel;
            }
        }
        return geofence;
    }
    public void addGeofence(Context context, GeofenceModel geofence, String goalName) {
        ArrayList<GeofenceModel> geofences = loadGeofences(context);
        if (geofences == null) {
            geofences = new ArrayList<>();
        }
        geofence.setKey(check(context));
        geofence.setGoalName(goalName);
        geofences.add(geofence);
        storeGeofences(context, geofences);
    }
    public void removeGeofence(Context context, GeofenceModel geofence) {
        ArrayList<GeofenceModel> geofences = loadGeofences(context);
        if (geofences != null) {
            if(geofences.remove(geofence)){
                Toast.makeText(context, geofence.getPlace()+" is removed", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(context, "Geofence is removed", Toast.LENGTH_SHORT).show();
            }
            storeGeofences(context, geofences);
        }
    }
    private String check(Context context) {
        ArrayList geofences = loadGeofences(context);
        Integer index;
        if(geofences == null){
            index = 0;
        }else{
            index = geofences.size();
        }
        return Integer.toString(index);
    }
}
