package id.web.adhiwie.ifthenplan.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.estimote.sdk.Beacon;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import id.web.adhiwie.ifthenplan.model.CueModel;

/**
 * Created by adhiwie on 20/10/2016.
 */

public class SharedPreferenceForTempBeacon {

    private static final String TAG = SharedPreferenceForTempBeacon.class.getSimpleName();

    private static final String PREFS_NAME = "tempBeaconPref";
    private static final String BEACON_TEMP = "tempBeacon";
    public SharedPreferenceForTempBeacon() {
        super();
    }

    public void storeTempBeacon(Context context, ArrayList<Beacon> beacons) {
        SharedPreferences sharedPref;
        SharedPreferences.Editor editor;
        sharedPref = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = sharedPref.edit();
        Gson gson = new Gson();
        String jsonFavorites = gson.toJson(beacons);
        editor.putString(BEACON_TEMP, jsonFavorites);
        editor.apply();
    }
    public ArrayList<Beacon> loadTempBeacon(Context context) {
        SharedPreferences sharedPref;
        ArrayList<Beacon> beacons;
        sharedPref = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        if (sharedPref.contains(BEACON_TEMP)) {
            String jsonCues = sharedPref.getString(BEACON_TEMP, null);
            Gson gson = new Gson();
            beacons = gson.fromJson(jsonCues, new TypeToken<List<Beacon>>(){}.getType());
        } else
            return null;
        return beacons;
    }
    public void addTempBeacon(Context context, Beacon beacon) {
        ArrayList<Beacon> beacons = loadTempBeacon(context);
        if (beacons == null) {
            beacons = new ArrayList<>();
        }
        beacons.add(beacon);
        storeTempBeacon(context, beacons);
    }
    public void removeTempBeacon(Context context, Beacon beacon) {
        ArrayList<Beacon> beacons = loadTempBeacon(context);
        if (beacons != null) {
            if(beacons.remove(beacon)){
                Toast.makeText(context, beacon.toString() + " is removed", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(context, "Beacon is removed", Toast.LENGTH_SHORT).show();
            }
            storeTempBeacon(context, beacons);
        }
    }
    public void clearTempBeacon(Context context) {
        SharedPreferences sharedPref;
        SharedPreferences.Editor editor;
        sharedPref = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = sharedPref.edit();
        editor.clear();
        editor.apply();
    }
    private void check(Context context) {
        ArrayList beacons = loadTempBeacon(context);
        Toast.makeText(context, Integer.toString(beacons.size()), Toast.LENGTH_SHORT).show();
    }
}
